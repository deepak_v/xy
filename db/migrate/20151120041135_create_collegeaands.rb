class CreateCollegeaands < ActiveRecord::Migration
  def change
    create_table :collegeaands do |t|
      t.string :one
      t.string :two
      t.string :three
      t.string :four
      t.string :five
      t.string :six
      t.date :seven
      t.date :eight
      t.text :nine
      t.text :ten
      t.text :eleven
      t.text :twelve
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
