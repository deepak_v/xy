require 'test_helper'

class SchoolesControllerTest < ActionController::TestCase
  setup do
    @schoole = schooles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:schooles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create schoole" do
    assert_difference('Schoole.count') do
      post :create, schoole: { eight: @schoole.eight, five: @schoole.five, four: @schoole.four, nine: @schoole.nine, one: @schoole.one, seven: @schoole.seven, six: @schoole.six, ten: @schoole.ten, three: @schoole.three, two: @schoole.two }
    end

    assert_redirected_to schoole_path(assigns(:schoole))
  end

  test "should show schoole" do
    get :show, id: @schoole
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @schoole
    assert_response :success
  end

  test "should update schoole" do
    patch :update, id: @schoole, schoole: { eight: @schoole.eight, five: @schoole.five, four: @schoole.four, nine: @schoole.nine, one: @schoole.one, seven: @schoole.seven, six: @schoole.six, ten: @schoole.ten, three: @schoole.three, two: @schoole.two }
    assert_redirected_to schoole_path(assigns(:schoole))
  end

  test "should destroy schoole" do
    assert_difference('Schoole.count', -1) do
      delete :destroy, id: @schoole
    end

    assert_redirected_to schooles_path
  end
end
